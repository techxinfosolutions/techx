<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'techx' );

/** MySQL database username */
define( 'DB_USER', 'techx' );

/** MySQL database password */
define( 'DB_PASSWORD', 'techxinfosolutions@987654321' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
**/

define('AUTH_KEY',         'I3ls>dX_8A{_A!cn*u7=mX%v#c-+@^2KN=O)p2P)Hgx^hElz9eP+f}/IBEI6Fe;7');
define('SECURE_AUTH_KEY',  '#iAMqs+/]UD-_B)PT+%OZ%!]?S(bf$-!:4/O1o%xv|7rZppeouyu;FB%sLijnqM}');
define('LOGGED_IN_KEY',    'H9nNO67>0G9@mWhb=!pIrCo;qBA}*(z*ZnFY>{/dP# PX+sD%HY(w}E/Mk|J9nnV');
define('NONCE_KEY',        'xi$txh+2:Vm$ xfye&;zhE4u|YEF^^clRrTWsC6(n}riXLRUcio3?rq{K.$,m|M`');
define('AUTH_SALT',        '$KYZEdGPNcV<;%u[REl6>O.r/2YD|;19AH/WZI_&xc62fF;w>4xO:is3.{n{*wc-');
define('SECURE_AUTH_SALT', 'Z#wO(5x1F^-3{N^|4l>{@%-L-wTY.%R&UL!JF4}#E~Lq1JZ2jHRzGH01qb^kA~s5');
define('LOGGED_IN_SALT',   'X56/-bUe>A7TkZO<8HRutBYPS9 UjUr95B72nO|~/R*O-0G$)y(}1<)K7+h>%][h');
define('NONCE_SALT',	   'mNh=lf0U&x#,+|xh, DNr-3++6b-IR9$hl}JH>Net)~zRYtoJ50%JLT&_5aj|>^J');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'techx_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
        define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
